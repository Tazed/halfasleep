package club.tazed.HalfAsleep.Commands;

import club.tazed.HalfAsleep.Config;
import club.tazed.HalfAsleep.HalfAsleepPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class ReloadConfigCommand implements CommandExecutor {

    private HalfAsleepPlugin plugin;
    public ReloadConfigCommand(HalfAsleepPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        plugin.reloadConfig();
        return false;
    }
}
