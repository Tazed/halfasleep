package club.tazed.HalfAsleep;

import club.tazed.HalfAsleep.Commands.ReloadConfigCommand;
import club.tazed.HalfAsleep.Events.BedEnter;
import club.tazed.HalfAsleep.Events.BedLeave;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class HalfAsleepPlugin extends JavaPlugin {
    private SleepWatcher sleepWatcher;
    private SleepWatcherTask sleepWatcherTask;
    private Config config;

    @Override
    public void onEnable() {
        loadConfig();
        registerWatcherTask();
        registerCommands();
        registerEvents();
    }

    private void loadConfig() {
        saveDefaultConfig();
        FileConfiguration fileConfig = createDefaultConfig();
        config = new Config(fileConfig);
    }

    private void registerWatcherTask() {
        sleepWatcher = new SleepWatcher(config);
        sleepWatcherTask = new SleepWatcherTask(sleepWatcher);
        getServer().getScheduler()
                .runTaskTimer(this, sleepWatcherTask, 0L, 20);
    }

    private void registerCommands() {
        getCommand("hareload").setExecutor(new ReloadConfigCommand(this));
    }

    private void registerEvents() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new BedEnter(), this);
        pluginManager.registerEvents(new BedLeave(), this);
    }

    @NotNull
    private FileConfiguration createDefaultConfig() {
        FileConfiguration fileConfig = getConfig();
        fileConfig.addDefault("sleepPercentage", 50);
        fileConfig.addDefault("sleepMillis", 3500);
        fileConfig.options().copyDefaults(true);
        saveConfig();
        return fileConfig;
    }

    public SleepWatcher getSleepWatcher() {
        return sleepWatcher;
    }

    public void reloadConfig() {
        reloadConfig();
        config.reload(getConfig());
    }
}
