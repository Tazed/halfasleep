package club.tazed.HalfAsleep.Events;

import club.tazed.HalfAsleep.HalfAsleepPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;

public class BedEnter implements Listener {
    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent event) {
        HalfAsleepPlugin plugin = HalfAsleepPlugin.getPlugin(HalfAsleepPlugin.class);

        if (event.getBedEnterResult() == PlayerBedEnterEvent.BedEnterResult.OK) {
            plugin.getSleepWatcher()
                    .addSleepingPlayer(event.getPlayer());
        }
    }
}
