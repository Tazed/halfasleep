package club.tazed.HalfAsleep.Events;

import club.tazed.HalfAsleep.HalfAsleepPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedLeaveEvent;

public class BedLeave implements Listener {
    @EventHandler
    public void onPlayerBedLeave(PlayerBedLeaveEvent event) {
        HalfAsleepPlugin plugin = HalfAsleepPlugin.getPlugin(HalfAsleepPlugin.class);
        plugin.getSleepWatcher()
                .removeSleepingPlayer(event.getPlayer());
    }
}
