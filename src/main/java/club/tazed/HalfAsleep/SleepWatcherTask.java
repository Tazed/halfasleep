package club.tazed.HalfAsleep;

import org.bukkit.Bukkit;
import org.bukkit.World;

/**
 * Checks status of players and sets time if needed.
 */
public class SleepWatcherTask implements Runnable {
    private SleepWatcher sleepWatcher;

    SleepWatcherTask(SleepWatcher sleepWatcher) {
        this.sleepWatcher = sleepWatcher;
    }

    public void run() {
        for (World world : Bukkit.getServer().getWorlds()) {
            if (sleepWatcher.checkIfWorldIsAsleep(world)) {
                Bukkit.broadcastMessage("Set time to day");
                world.setTime(0);
            }
        }
    }
}
