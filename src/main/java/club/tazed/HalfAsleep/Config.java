package club.tazed.HalfAsleep;

import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    private FileConfiguration config;

    public Config(FileConfiguration config) {
        this.config = config;
    }

    public void reload(FileConfiguration config) {
        this.config = config;
    }

    public int getSleepMillis() {
        return config.getInt("sleepMillis");
    }

    public int getSleepPercentage() {
        return config.getInt("sleepPercentage");
    }
}
