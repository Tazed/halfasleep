package club.tazed.HalfAsleep;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Tracks every players sleeping state
 */
public class SleepWatcher {
    private Map<Player, LocalTime> sleepingPlayers;
    private Config config;

    SleepWatcher(Config config) {
        sleepingPlayers = new HashMap<>();
        this.config = config;
    }

    /**
     * Checks if there are enough players currently sleeping
     *
     * @param world
     * @return
     */
    public boolean checkIfWorldIsAsleep(World world) {
        int playerCountInWorld = getPlayerCountInWorld(world);
        int playerCountRequired = getPlayerCountRequired(world);
        int playerCountSleeping = getPlayerCountSleeping(world);

        if (playerCountInWorld == 0) {
            return false;
        }

        return playerCountRequired <= playerCountSleeping;
    }

    /**
     * Adds the player to the sleeping list
     *
     * @param player
     */
    public void addSleepingPlayer(Player player) {
        if (!sleepingPlayers.containsKey(player)) {
            sleepingPlayers.put(player, LocalTime.now());

            Bukkit.getServer()
                    .broadcastMessage(String.format("%s is now sleeping [%d/%d]",
                            player.getName(),
                            getPlayerCountInBed(player.getWorld()),
                            getPlayerCountRequired(player.getWorld())));
        }
    }

    /**
     * Removes the player from the sleeping list
     *
     * @param player
     */
    public void removeSleepingPlayer(Player player) {
        if (sleepingPlayers.containsKey(player)) {
            sleepingPlayers.remove(player);

            // Don't print after player
            if (player.getWorld().getTime() > 6000) {
                Bukkit.getServer()
                        .broadcastMessage(String.format("%s is no longer sleeping [%d/%d]",
                                player.getName(),
                                getPlayerCountInBed(player.getWorld()),
                                getPlayerCountRequired(player.getWorld())));
            }
        }
    }

    /**
     * Get the number of online players in the world.
     *
     * @param world
     * @return
     */
    private int getPlayerCountInWorld(@org.jetbrains.annotations.NotNull World world) {
        return world.getPlayers().size();
    }

    /**
     * Get the number of players required to set the time to day.
     *
     * @param world
     * @return
     */
    private int getPlayerCountRequired(World world) {
        int playerCountInWorld = getPlayerCountInWorld(world);
        return (int) Math.ceil((playerCountInWorld / 100f) * config.getSleepPercentage());
    }

    /**
     * Returns the number of players currently in bed
     *
     * @param world
     * @return
     */
    private int getPlayerCountInBed(World world) {
        return (int) sleepingPlayers.keySet()
                .stream()
                .filter(player -> player.getWorld() == world)
                .count();
    }

    /**
     * Returns the number of players in bed longer than <code>config.getSleepMillis()</code> milliseconds
     *
     * @param world
     * @return
     */
    private int getPlayerCountSleeping(World world) {
        return (int) sleepingPlayers.keySet()
                .stream()
                .filter(player -> player.getWorld() == world)
                .filter(player -> {
                    int sleepMillis = config.getSleepMillis();
                    return sleepingPlayers.get(player)
                            .compareTo(LocalTime.now().minusSeconds(sleepMillis / 1000))
                            <= 0;
                })
                .count();
    }
}
